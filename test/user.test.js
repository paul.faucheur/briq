const User = require('../models').user;

test('A user has a default balance of 0', () => {
  const user = User.build();
  expect(user.balance).toEqual(0);
});

test('A user tries to give a negative amount of briqs to another user', async () => {
  //function is suposed to throw an error
  await expect(User.give(-56, 5)).rejects.toThrow('error');
});

test('A user tries to give more briqs than he has to another user', async () => {
  //function is suposed to throw an error
  await expect(User.give(25000000000, 5)).rejects.toThrow('error');
});

test('A user tries to give a good amount of briqs to a user that doesn t exist', async () => {
  //function is suposed to throw an error
  await expect(User.give(25, 565656556565655665)).rejects.toThrow('error');
});
